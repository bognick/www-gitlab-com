---
layout: job_family_page
title: "Product Marketing Manager"
---
Product marketing managers at GitLab have a unique opportunity to contribute to our [mission](https://about.gitlab.com/company/strategy/#mission):

To change all creative work from read-only to read-write so that everyone can contribute.
When everyone can contribute, consumers become contributors, and we greatly increase the rate of human progress.

Product marketing managers at GitLab help tell the story about the value of how GitLab helps to unlock the potential of sharing and contribution.   As a product marketing manager, you understand our customers, our community, and the challenges they face.  You write about their story and help them understand how GitLab can help them become the heroes they really are.

We work in a unique way at GitLab, where flexibility and independence meet a high paced, pragmatic way of working. And everything we do is in the open. We recommend looking at our [product marketing handbook](/handbook/marketing/product-marketing/) to get started.

## Responsibilities

#### Who, Where and Why
* Research and define our enterprise buyer and user personas, their specific needs, challenges, and goals that we can help address.
* Research, document, and operationalize particular customer use cases and problems that GitLab solves.
* Identify and define the key value drivers which differentiate GitLab in the market.
* Partner with sales, product management, customer success, and engineering teams to refine and evolve our product roadmap.

#### What
* Develop product marketing collateral such as website pages, blogs, white papers, webinars, videos, decks, and data sheets supporting the buyer's journey communicating the value of GitLab.
* Participate and contribute to field and industry events as a GitLab evangelist and thought leader.
* Develop and deliver product, market, and industry-specific enablement to the GitLab sales team and partners.
* Create campaign messaging and content as required to support ongoing marketing initiatives.

#### Proof
* Develop strong relationships with analysts and regularly brief them about GitLab’s unique value proposition.
* Document and demonstrate GitLab value in analyst market research reports.
* Support the development of customer case studies and webinars that help to demonstrate the value of GitLab for our customers.

#### Choices
* Participate and support win/loss analysis to understand why we win, where we win, who to target, and what to avoid.
* Support research into competitive threats and challenges to understand how to position GitLab in a rapidly changing market.
* Research and track overarching market trends and patterns that influence our ability to compete.


## Requirements
* 3-4 years of experience in product marketing with desired experience in the specialty.
* Technical background or good understanding of Application Development and IT operations; familiarity with Git, Continuous Integration, Containers, and Kubernetes a plus.
* Outstanding English communications skills with the ability to explain and translate complex technical concepts into simple and intuitive communications.
* Able to coordinate across many teams and iterate and deliver in a fast-moving startup environment.
* Proven ability to be self-directed and work with minimal supervision.
* Data-driven, use data to measure results and inform decision making and strategy development.
* Experience with Software-as-a-Service offerings and open core software a plus.
* Experience designing sales collateral from scratch based on sales conversations, sales calls, product interviews, user interviews, market research, and your own experience.
* You share our [values](/handbook/values) and work according to those values.
* Ability to use GitLab

## Senior Product Marketing Manager
Senior product marketing managers are leaders and bring deep expertise in a specific domain. Through your leadership, experience, and insight, you will position GitLab to address business challenges, contribute to the product strategy, and provide thought leadership. You will be expected to prioritize and manage your work with minimal guidance from leadership or other product team members.

###  SR Product Marketing Managers also
* Develops and implements GTM strategies and campaigns in partnership with sales, growth, digital, and content marketing.
* Researches and supports pricing and packaging decisions in order to meet market demands and needs.
* Defines and leads market research projects into specific market segments.

### SR PMM Requirements
* At least 6 years enterprise software marketing experience, including 4 directly in product marketing.
* Deep understanding of Agile and DevOps methodologies across the entire Software Development Lifecycle, especially in large enterprise environments.
* Extensive experience in at least one of the following domains/areas (portfolio management, requirements management, agile project management, source code management, continuous integration, quality management, application security, continuous delivery, containers, kubernetes, release management, or application performance monitoring)
* Outstanding English communication skills with the ability to explain and translate complex technical concepts into simple and intuitive communications.
* Able to coordinate across many teams and iterate and deliver in a fast-moving startup environment.
* Proven ability to be self-directed and work with minimal supervision.
* Data-driven, use data to measure results and inform decision making and strategy development.
* Experience with Software-as-a-Service offerings and open core software a plus.
* Experience designing sales collateral from scratch based on sales conversations, sales calls, product interviews, user interviews, market research, and your own experience.
* You share our [values](/handbook/values) and work according to those values.

## Specialties

### Security Specialist
As a Product Marketing Manager, Security Specialist, you will work closely with our marketing, engineering, business development and sales team, and partners to help them understand how GitLab security capabilities solve customer problems as well as educate them about market competitors.  You will also be responsible for crafting, testing, creating and rolling out messaging and positioning of GitLab’s security capabilities.

#### Requirements
* At least 5 years of product marketing experience marketing cybersecurity products and services such as application security, endpoint security, SIEM, security analytics or network security domains.
* Understand market dynamics, the competition, and customer needs of the cybersecurity space.
* Deep understanding of Agile and DevOps methodologies across the entire Software Development Lifecycle, especially as it relates to application security risks, threats, trends, and patterns.
* Understand enterprise buyer and user personas tasked with security challenges and pain points and how to market security solutions to them.
* Security certifications such as CISSP a plus.


### Monitoring Specialist
As a Sr. Product Marketing Manager, Monitoring Specialist, you will work closely with our marketing, engineering, business development, and sales team, and partners to help them understand how GitLab monitoring capabilities solve customer problems as well as educate them about market competitors.  You will also be responsible for crafting, testing, creating and rolling out messaging and positioning of GitLab’s monitoring capabilities.

#### Requirements
* At least 5 years of product marketing experience marketing IT operations monitoring products and services such as tracing, logging, application performance monitoring, incident management, and related metrics.
* Understand market dynamics, the competition, and customer needs of the enterprise IT operations space.
* Understand enterprise buyer and user personas tasked with enterprise IT operations and monitoring challenges and pain points and how to market monitoring solutions to them.
* Experience with cloud native, datacenter, and hybrid infrastructure and applications desirable.
* Operations certifications such as ITIL a plus.


### Developer Specialist (create)
As a Sr.  Product Marketing Manager, development, you will work closely with our marketing, engineering, business development, and sales team, and partners to help them understand how GitLab source code management capabilities and developer features help solve customer problems as well as educate them about market competitors.  You will also be responsible for crafting, testing, creating and rolling out messaging and positioning of GitLab’s scm capabilities.

#### Requirements
* At least 5 years of product marketing experience marketing developer products and services.  such as source code management, api management, review management, code quality, developer metrics, and productivity.
* Understand market dynamics, the competition, and customer needs of the enterprise application developers.
* Understand enterprise buyer and user personas tasked with developer tools and solutions and how to market developer solutions to them.
* Experience with git, perforce, bitbucket, Gerrit, code reviews, pair programming, and open source software.

### Verify Specialist (Continuous Integration)
As a Product Marketing Manager, Verify Specialist, you will work closely with our marketing, engineering, business development, and sales team, and partners to help them understand how GitLab continuous integration, testing, and quality management capabilities help solve customer problems as well as educate them about market competitors.  You will also be responsible for crafting, testing, creating and rolling out messaging and positioning of GitLab’s CI capabilities.

#### Requirements
* At least 5 years of product marketing experience marketing IT software development lifecycle automation and quality products and services such as build automation, test automation, quality management, and agile testing.
* Understand market dynamics, the competition, and customer needs of the enterprise application developers and software testing / QA.
* Understand enterprise buyer and user personas tasked with both developer and QA tools and solutions and how to market to them.
* Experience with jenkins, bamboo, circle CI, automated testing, quality management, traceability and cloud-native applications.


### Agile-Planning Specialist (Plan)
As a Product Marketing Manager, Agile-Planning Specialist, you will work closely with our marketing, engineering, business development, and sales team, and partners to help them understand how GitLab planning and portfolio management capabilities help solve customer problems as well as educate them about market competitors.  You will also be responsible for crafting, testing, creating and rolling out messaging and positioning of GitLab’s agile project management capabilities.

#### Requirements
* At least 5 years of product management or product marketing experience marketing IT software agile planning products and services.
* Understand market dynamics, the competition, and customer needs of the enterprise agile project and portfolio management.
* Understand enterprise buyer and user personas tasked with both project and product manager tools and solutions and how to market to them.
* Experience with Jira, Rally, Version One, Agile Craft and other planning and portfolio mgt tools.


### Partner and Channel Marketing
As the Partner Product Marketing Manager, you will work with our marketing, business development and sales teams, to develop and execute global go-to-market strategies and programs to drive customer acquisition and revenue growth for GitLab. Reporting to the Director, Product Marketing, you will have a global charter to enable partners and resellers within our growing partner and channel ecosystems. In addition, you’ll be responsible for crafting and implementing co-marketing campaigns targeting developers and IT professionals, to increase awareness and adoption of GitLab. You will work to shape go-to-market messaging and strategy for new offerings with partners and work with partners and resellers to build an effective and scalable ecosystem.

#### Requirements

#### Channel Marketing:
* Develop our worldwide channel marketing strategy, and train the field marketing team on regional execution that maximizes our reach and scale with channel partners.
* Implement channel marketing initiatives, programs, communication vehicles, and sales tools to drive increased market adoption and channel revenue goals.
* Spearhead enablement activity with resellers to drive end-user adoption, and manage MDF program.
* Assist sales team in the development of actionable and measurable programs for our channel partners.
* Work with field marketing to execute channel marketing programs, regionally.
* Accurately track activity performance and provide well-informed recommendations on future resource and budget allocation.
* Assist in the preparation of reports to help evaluate activity and reseller effectiveness, conversion rate, and relative performance.
* Market to, and through our channel. Ensure prospective resellers understand the value of partnering with GitLab.
* Complete other duties as assigned to meet company channel goals.

#### Partner Marketing:
* Initiate and develop co-marketing initiatives to execute in tandem with our partners.
* Market the benefit of our technology partnerships to all GitLab prospects.
* Develop and execute marketing programs when announcing new technology partnerships.
* Develop sales enablement programs for new partnerships, training the sales team on added functionality and associated benefits.
* Develop an ROI performance analysis for partner marketing initiatives and utilize findings to tailor future initiatives.
* Develop outreach plan for existing partnerships maintaining a steady development of joint content and demand generation activities.
* Create partnership positioning program for a cloud native approach and associated ecosystem.

### Market Research and Customer Insights
As a Product Marketing Manager, Market Research and Customer Insights, you will bring your market knowledge and experience to help shape our go to market strategy, messaging and execution.  You will partner closely with our marketing, engineering, business development, sales team, and partners to help them address the unique requirements of the DevOps market.

#### Requirements
* Conduct research on current and future Enterprise IT trends through written reports, surveys, interviews and interactions with industry analysts, industry conferences, self-study, and interactions with our customers to develop key insights for GitLab.
* Help GitLab develop a deep knowledge and understanding of enterprise buyers including their challenges and goals that help them drive success for their companies.
* Establish deep relationships with GitLab customers to understand their strategy and operational challenges.
* Evaluate, analyze and present research results in a consumable and actionable format - whitepapers, web pages, case studies, conference presentations, sales enablement sessions, and executive presentations.
* Help establish GitLab as a thought leader in the DevOps space.
* Participate and help prepare for industry analyst briefings as required.
* Support support to sales team in sales discussions with prospects and customers.
* Past experience in market and customer research roles is a plus.

## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).

* Qualified candidates will be invited to schedule a [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
* Candidates then may be offered a 45 minute interview with our Chief Marketing Officer
* Next, candidates will be invited to schedule a interview with the Senior Director of Marketing and Sales
* Candidates will then be invited to schedule 45 minute interviews with our Regional Sales Director of the US East Coast
* After, candidates may be invited to schedule a 45 minute interview with the Vice President of Product
* Next, candidates will be invited to schedule a interview with the Chief Revenue Officer.
* Finally, our CEO may choose to conduct a final interview.
* Successful candidates will subsequently be made an offer via email.

Additional details about our process can be found on our [hiring page](/handbook/hiring).
