---
layout: markdown_page
title: Is it any good?
suppress_header: true
---

## Why is this page called 'Is it any good?'

When people hear about a new product they often ask if it is any good. [A Hacker News user once wrote](https://news.ycombinator.com/item?id=3067434): "Starting immediately, all raganwald projects will have a 'Is it any good?' section in the readme, and the answer shall be 'yes'." We were inspired and added that section to the [GitLab readme](https://gitlab.com/gitlab-org/gitlab-foss/blob/master/README.md#is-it-any-good).

Don't only take our word for it! GitLab is the only comprehensive DevOps platform being delivered as a single application, and our unique breadth in DevOps has enabled us to enjoy similarly [broad coverage across analysts’ categories and reports](/analysts/). 

The GitLab solution is the result of collaboration between [thousands of community contributors](http://contributors.gitlab.com/), based on feedback from the over 100,000 organizations using GitLab to power their DevOps transformations. The GitLab community contributes code, documentation, translation, design, and product ideas based on their real world challenges, making GitLab more useful and valuable every day.

Because of the community behind GitLab, in many cases we see rapid adoption through better developer experience and collaboration. We've [liked thousands of supportive tweets](https://twitter.com/gitlab/likes) from [our 100,000+ followers on Twitter](https://twitter.com/gitlab/).

This page shares more of the many acknowledgements GitLab has enjoyed.

## On this page
{:.no_toc}

- TOC
{:toc}

## GitLab is DevOps.com's 'Best DevOps Solution Provider'

[![DevOps Dozen Best Solution Provider graphic](/images/blogimages/DevOpsDozen2019SolutionProvider-500x250.png){: .small.left.wrap-text}](https://devops.com/5th-annual-devops-dozen-winners-announced/)

GitLab was [awarded the DevOps Dozen award for Best DevOps Solution Provider by industry leaders MediaOps and DevOps.com](https://devops.com/5th-annual-devops-dozen-winners-announced/), who wrote: "GitLab has really separated itself from the pack, from the breadth of its vision for an end-to-end solution to the many different ways that GitLab operates as a company."

"But it is not just that GitLab is different; GitLab has captured a broad swath of the market who feel bought into the company."

Once we had been chosen as finalists, in true GitLab fashion we used GitLab.com to openly create and share [our case for the top prize, still available as a page in our handbook](/handbook/marketing/product-marketing/dod12/2019-Best-Solution/).

## GitLab recognized as a 451 Firestarter by 451 Research

[![GitLab recognized by 451 Research as a 451 Firestarter](/images/blogimages/451FIRESTARTER2019.png){: .small.right.wrap-text}](/press/releases/2020-01-14-gitlab-recognized-as-451-firestarter.html)

[GitLab received a 451 Firestarter award from leading technology research and advisory firm 451 Research](/press/releases/2020-01-14-gitlab-recognized-as-451-firestarter.html), recognizing exceptional innovation within the information technology industry.

"451 Research has built its reputation on helping organizations understand innovation and disruption in the enterprise IT industry, and the Firestarter award is one of the ways we spotlight important trends and players," said Jay Lyman, 451 Research Principal Analyst.

"Innovative approaches from companies such as GitLab — with its open source software technology and community contributions as well as transparency with customers — merit the recognition of a 451 Firestarter award."

## GitLab ranked above GitHub as a top developer tool

[![GitLab ranked above GitHub in Axosoft top 20 Dev Tools for 2019](/images/blogimages/axosoft-top-20-devtools-2019.png){: .small.left.wrap-text}](https://blog.axosoft.com/top-developer-tools-2019/)

The results of the [Axosoft 2019 survey of over 1,000 software engineers from elite organizations across the world](https://blog.axosoft.com/top-developer-tools-2019/) ranked GitLab in the top 20 developer tools. As they put it, "We could have put together our own list of the best development tools, but for the last two years, we’ve asked our community instead."

"GitLab is giving GitHub a run for its money! GitLab climbed the ranks 4 spots and overtook GitHub for the first year. While GitHub had the most significant drop in rankings from number 3 to number 11 year-over-year."

## GitLab CI is a leader in the The Forrester Wave™

[![Forrester Wave graphic](/images/home/homepage-forrester-wave-2019.png){: .small .margin-top20 .margin-bottom20}](/resources/forrester-wave-cloudnative-ci/)

[Forrester evaluated GitLab as a Leader in Cloud-Native Continuous Integration in The Forrester Wave™: Cloud-Native Continuous Integration Tools, Q3 2019 report.](/resources/forrester-wave-cloudnative-ci/)

As the report states, “GitLab’s simple and cohesive approach lands it squarely as a leader.”

## GitLab recognized as an IDC Innovator in Tools Supporting Open Developer Platforms

[![GitLab recognized as an IDC Innovator in Tools Supporting Open Developer Platforms](/images/blogimages/IDCInnovator2019.png){: .small.right.wrap-text}](https://www.idc.com/getdoc.jsp?containerId=US45074219)

[GitLab was recognized as an IDC Innovator in Tools Supporting Open (Unopinionated) Developer Platforms](https://www.idc.com/getdoc.jsp?containerId=US45074219) in May, 2019 by International Data Corporation.

IDC Innovators are emerging vendors "that have demonstrated either a groundbreaking business model or an innovative new technology — or both."

## GitLab has YoY growth in adoption of version control services study, while GitHub and Bitbucket both decline

[![YoY Adoption of Version Control Services Graph](/images/blogimages/yoy-adoption-vcs-2018-2019.svg){: .medium .margin-top20 .margin-bottom20}](https://www.jetbrains.com/lp/devecosystem-2019/)

[Source: The State of Developer Ecosystem 2019 - JetBrains](https://www.jetbrains.com/lp/devecosystem-2019/)

## GitLab ranked number 4 software company (44th overall) on Inc. 5000 list of 2018's Fastest Growing Companies

[![GitLab ranked number 4th fastest-growing private software company on the Inc. 5000 list of 2018's Fastest Growing Companies](/images/blogimages/inc-5000-2018-no4-software.png){: .small.right.wrap-text}](/blog/2018/08/16/gitlab-ranked-44-on-inc-5000-list/)

In 2018, GitLab was America's [4th fastest-growing private software company and 44th overall on the Inc. 5000 list](/blog/2018/08/16/gitlab-ranked-44-on-inc-5000-list/) with revenue growth of 6,213 percent over the previous three years. 2018 was the first year GitLab appeared on the Inc. 5000 list.

The 2018 Inc. 5000 ranking system was based on the percentage of revenue growth qualifying companies saw from 2014 to 2017. For consideration, companies needed to be private, for-profit, independent and U.S.-based as of December 31, 2017. The companies must have also been incorporated by March 31, 2014 with a minimum revenue of $200,000 for that year and $2 million for 2017.

## GitLab is a top 3 innovator in IDC's list of Agile Code Development Technologies for 2018

[![IDC innovators banner](/images/logos/idc-innovator.png){: .margin-top20 .margin-bottom20}](/analysts/idc-innovators/)

[IDC recognized GitLab as a top 3 IDC Innovator in Agile Code Development Technologies](/analysts/idc-innovators/) after considering an overall assessment, key differentiators, challenges in the market, and related research.

## GitLab is a strong performer in the new Forrester Value Stream Management Tools 2018 Wave Report

[![Forrester VSM Wave graphic](/images/home/forrester-vsm-graphic.png){: .small .margin-top20 .margin-bottom20}](/analysts/forrester-vsm/)

[Forrester evaluated GitLab as a strong performer for VSM capabilities on top of its end to end DevOps capabilities.](/analysts/forrester-vsm/)

Forrester's assessment included: "GitLab combines end-to-end capability with the power of open source. GitLab offers a DevOps tool for each step of the software development process. Top-level views sitting across these tools provide its VSM functionalities."

They added: "GitLab is best for companies that are looking for a broad, integrated solution. Organizations that want a comprehensive VSM solution that can also serve as their DevOps tool chain will really appreciate GitLab. GitLab's vision is firmly open source and dev-centric; companies that live and breathe dev-first will appreciate this approach."

## GitLab has been voted as G2 Crowd Leader

[![G2 gitlab image](/images/logos/gitlab-g2.svg){: .small .margin-top20 .margin-bottom20}](https://www.g2crowd.com/products/gitlab/reviews)

[GitLab was voted a G2 Crowd Leader based on more than 170 public reviews with a 4.4 rating](https://www.g2crowd.com/products/gitlab/reviews), with those including: _"[Powerful team collaboration tool for managing software development projects](https://www.g2.com/products/gitlab/reviews/gitlab-review-1976773)," "[Great self-hosted, open source source control system](https://www.g2.com/products/gitlab/reviews/gitlab-review-436746)," "[Efficient, can trace back, easy to collaborate](https://www.g2.com/products/gitlab/reviews/gitlab-review-701837)," and "[Perfect solution for cloud and on-premise DevOps tool](https://www.g2.com/products/gitlab/reviews/gitlab-review-2388492)."_

## GitLab has 2/3 market share in the self-managed Git market

With more than 100,000 organizations self-hosting GitLab, we have the largest share of companies who choose to host their own code. We’re estimated to have two-thirds of this single-tenant market.

When [Bitrise surveyed](http://blog.bitrise.io/2017/01/27/state-of-app-development-in-2016.html#self-managed) ten thousand developers who build apps regularly on their platform, they found that 67 percent of self-managed apps prefer GitLab’s on-premise solution.

[![Image via Bitrise blog](/images/blogimages/bitrise-self-hosted-chart.png){: .medium}](http://blog.bitrise.io/2017/01/27/state-of-app-development-in-2016.html#self-managed)<br>

Similarly, in their survey of roughly one thousand development teams, [BuddyBuild found](https://www.buddybuild.com/blog/source-code-hosting#selfhosted) that 79% of mobile developers who host their own code have chosen GitLab:

[![Image via buddybuild blog](/images/blogimages/buddybuild-self-hosted-chart.png){: .medium}](https://www.buddybuild.com/blog/source-code-hosting#selfhosted)<br>

In their articles, both Bitrise and BuddyBuild note that few organizations use self-managed instances. We think there is a [selection effect](https://en.wikipedia.org/wiki/Selection_bias), because both of them are SaaS-only offerings.

Based on our experience, the vast majority of enterprises ([organizations with 2,000+ employees](/handbook/business-ops/resources/#segmentation)) self-host their source code server, frequently on a cloud service like AWS or GCP, instead of using a SaaS service.

Another assumption is that Git is the most popular version control technology for the enterprise. Certainly not every enterprise has completely switched yet, but it does seem that Git is [almost 10 times larger than SVN and Mercurial](https://trends.google.com/trends/explore?q=git,svn,perforce,mercurial,tfs).

Per the dominance of Git, the SCM market is lacking analysis that is as complete, independent, and directly relevant to the question of market share as GitLab would prefer, but with these qualifications and multiple sources, we find the data and discussions sufficient to share. GitLab has encouraged additional, independent analysts to address the SCM market and will continue to do so.

## GitLab CI is the fastest growing CI/CD solution

In his post on [building Heroku CI](https://blog.heroku.com/building-tools-for-developers-heroku-ci) in mid 2017, Heroku’s Ike DeLorenzo noted "GitLab is surging in VCS and CI (and in developer buzz)" and that GitLab CI is “clearly the biggest mover in activity on Stack Overflow,” with more popularity than both Travis CI and CircleCI.

GitLab was the second most popular CI system by the end of that year, in [a report on cloud trends from Digital Ocean](https://assets.digitalocean.com/currents-report/DigitalOcean-Currents-Q4-2017.pdf).

[![Image via Digital Ocean](/images/ci/gitlab-popular-ci.png){: .medium}](https://assets.digitalocean.com/currents-report/DigitalOcean-Currents-Q4-2017.pdf)

GitLab's commitment to seamless integration extends to CI itself. Integrated CI/CD is more time and resource efficient than a set of distinct tools, and allows developers greater control over build pipelines, so they can spot issues earlier and address them at a lower cost. Tighter integration between different stages of the development process makes it easier to cross-reference code, tests, and deployments while discussing them, allowing you to see more context and iterate more rapidly.

We've heard from customers like [Ticketmaster](/blog/2017/06/07/continous-integration-ticketmaster/) that adopting GitLab CI can transform the entire software development lifecycle (SDLC), in their case helping the Ticketmaster mobile development team deliver on the longstanding goal of weekly releases. As more companies look to embrace CI as part of their development methodology, having CI fully integrated into their overall SDLC solution will ensure these companies are able to realize the full potential of CI. You can read more about the benefits of integrated CI in our white paper, [Scaling Continuous Integration](/resources/whitepaper-scaled-ci-cd/).

## GitLab is one of the top 30 open source projects

The Cloud Native Computing Foundation placed us in the top right quadrant of their [Highest Velocity Open Source Projects](/blog/2017/07/06/gitlab-top-30-highest-velocity-open-source/) in 2017, by measuring GitLab commits, authors, issues, and merge requests.

## Our customers love sharing their successes

GitLab customers are often asked to speak at events. [Hear their stories and learn from their experiences](/customers/marketplace/).

## GitLab has more than 2,000 contributors

[Our nearly 3,000 contributors have submitted more than 8,000 merge requests](http://contributors.gitlab.com/), including those from contributing customers and organizations such as [Siemens](/blog/2018/12/18/contributor-post-siemens/) and [CERN](/blog/2016/11/23/gitlab-adoption-growing-at-cern/).
