---
layout: handbook-page-toc
title: "People Group Metrics"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Reporting

The People Group pulls metrics reports each month, quarter, and year, as well as routinely for the People Team [Group Conversation](/handbook/people-group/group-conversations/).

The monthly metrics reports can be found in the [Google Drive](https://drive.google.com/drive/u/1/folders/1UNisqJAJQbYiEplNKG0FsgKQkx4-qoHA) and is only accessible to those who contribute to and review the reports, as they contain confidential information about team members and candidates. The reports contain several tabs: "Summary", "Recruiting", "SAT", "Diversity", "Low Location Factor", and "Turnover". The  team is responsible for the the Low Location Factor and Turnover tabs, as well as summarizing their findings on the Summary tab. The reports are done in the month following the month that we are analyzing so that we are able to have the full picture (e.g. January's metrics report is done in February).


## Average Location Factor

The average location factor of all team members per department or division. The location factor directly correlates to geographical area in the [Compensation Calculator](/handbook/people-group/global-compensation/#compensation-calculator). The company wide average location factor target is < 0.65. Each division and department also has their own average location factor target. 
<embed width="100%" height="100%" src="<%= signed_periscope_url(chart: 6238450, dashboard: 482006, embed: 'v2') %>">


|   Division  |   Target  |
| ----------- | --------- |
| Engineering |   0.58    |
| G&A         |   0.69    |
| Marketing   |   0.72    |
| Meltano     |   0.67    |
| Product     |   0.72    |
| Sales       |   0.72    |
|**Grand Total** |  **0.65**   |

|   Department  |   Target  |
| ----------- | --------- |
| Accounting  |   0.68       |
| Business Development   |   0.82    |
| Business Operations |   0.70    |
| CEO         |   1.00    |
| Channel   |   0.86    |
| Commercial Sales     |   0.69    |
| Corporate Marketing       |   0.79    |
| Customer Solutions       |   0.74    |
| Customer Success       |   0.69    |
| Customer Support       |   0.61    |
| Demand Generation       |   0.67    |
| Development      |   0.54    |
| Digital Marketing       |   0.80    |
| Enterprise Sales       |   0.74    |
| Field Marketing       |   0.71    |
| Field Operations      |   0.89    |
| Finance       |   0.73    |
| Infrastructure       |   0.58    |
| Legal                |   0.69    |
| Marketing Ops       |   0.71    |
| Outreach      |   0.66    |
| People Ops      |   0.72    |
| Product Management     |   0.72    |
| Product Marketing       |   0.79    |
| Quality      |   0.58    |
| Recruiting      |   0.62    |
| Security      |   0.66    |
| UX      |   0.63    |
|**Grand Total** |  **0.65**   |

## Low Location Factor Reporting

1. Create the "Low Location Factor Factor Data" tab.
1. In columns A through E add the following information from the "Comp Data Analysis & Modeling" Google spreadsheet.
  * Employee ID
  * First Name
  * Last Name
  * City
  * Location Factor
  * In Range? (Metrics)
1. In columns H and on export and past the "Low Location Factor" Report from BambooHR. Remove anyone who was hired after the last day of the reporting month and ensure any terminated team members that were active as of the last day of the month are accounted for.
1. Run an audit to match Employee IDs and Cities from the comp calc to BambooHR and correct any misaligned information
1. Copy any new hires for the reporting month into the rows above as we will report on just that month's metrics separately.
1. Next, update the rolling three month's tab by adding this month's new hires and removing the new hires from what would be four months ago.
1. Generate pivot tables on a new tab totaling the average location factor for each department and division for the reporting month, rolling last three months, and company wide average location factors.
1. Update and audit the figures on the "Location Factor Graphs/Summary" tab which will automatically update the graph.
1. Copy and paste the graph to the summary page outlining anything of importance based on the analytics.
1. Update the slide deck with this information as needed.

## Percent Over Compensation Band

During the Location Factor reporting, People Operations Analysts will also analyze against how many team members in a division or department are compensated above the bands specific by our [Global Compensation](/handbook/people-group/global-compensation/#compensation-principles) policy. To determine this, use the "In Range? (Metrics)" column from the Low Location Factor Reporting and generate a pivot table using a count of "FALSE" per department and division. Add this information to the "Location Factor Graphs/Summary" tab to generate a percentage based on total headcount per department and division as well as the raw number. The number can help explain the percentage if a department or division is small, for example.

The percent over compensation band cap is <= 1%.

TODO: Weight the percentage based on how far out of range a team member is. The Compensation and Benefits Manager will synch with the Finance Business Partner to present to the CFO.
Note: The percentages may fluctuate based on the results of our Compensation Review through Radford. If we change the compensation bands, we are also adjusting whether someone falls in or out of range.

## Onboarding Satisfaction (OSAT)

New team member feedback of the onboarding experience in a given month.
The Onboarding Satisfaction target is > 4.
Read more about [how we measure satisfaction at GitLab](/handbook/business-ops/data-team/metrics/#satisfaction).

<embed width="100%" height="100%" src="<%= signed_periscope_url(chart: 6873595, dashboard: 482006, embed: 'v2') %>">

## Onboarding task completion < X (TBD)

Tracking the days it takes to complete onboarding task completion from the day the onboarding task is open to the day all People Group tasks are complete on the onboarding task. The target is still to be determined.

## Ship X% of work scope within agreed timeframe

Ensuring the timeframe that is set for each specific work scope is met.

## Proposals defined for each issue = 100%

Triaging issues in the [People Operations Engineering Issue Tracker](https://gitlab.com/gitlab-com/people-group/people-operations-engineering) and ensuring they are ready for development.

## Turnover

There are two parts to the turnover reporting: Turnover and Tenure.

1. Open the "Turnover Rates Workbook" google sheet.
1. Update the 2019 Term Data tab by running the "Additions and Terminations" Report in BambooHR for the reporting month. Populate the following columns, Time in Job, MOS Category,	Type,	Tenure in days. Carry down the average tenure and rolling 12 months tenure.
1. Update the Rolling Turnover tab.
  * Delete what would be the 13th month and add a new item in for the reporting month under rolling tenure and rolling turnover by month.
  * Pull the Turnover Report from BambooHR for the last rolling twelve months and update the information in the spreadsheet.
1. Update the 2019 Average Tenure Data Report
  * Pull the Average Tenure Data report from BambooHR for all active team members. Ensure anyone hired after the reporting month is removed and any terminated team members that were active at the end of the reporting month are included.
  * Add in the end date, and carry down the formula for tenure in days and the transfer to tenure in months/years.
  * Populate the summary box and transfer that data to the Rolling Turnover tab.
1. Copy and paste the table from the Rolling Turnover tab to the Turnover tab on the Metrics Reporting spreadsheet and the GitLab Turnover Rates workbook (viewable to the company)
1. Update the information on the Turnover tab: Voluntary/Involuntary term information (tag the HRBPs to update reasons), Tenure, and term percentages which update the graphs.
1. Add this information and a copy of the graphs to the summary tab and the slide deck as needed.

## Team Members

For calculating KPIs we define Team Members on the date measured as the number of full time equivalent employees or contractors who are providing services to GitLab and are listed on our Team page.
Excluded from this category are board members, board observers, core team members, and advisors.
The canonical source of truth of the number of team members comes from BambooHR.

## Team Member Voluntary Turnover

Voluntary turnover is any instance in which a team member actively chooses to leave GitLab. GitLab measures voluntary turnover over a rolling 12 month period, as well as over a calendar month.  (The default period is over a rolling 12 month period.) The rolling 12 month voluntary team member turnover target is < 10%. In order to achieve the rolling 12 month voluntary team member turnover target, the monthly voluntary team member turnover target is < 0.83% (10/12). The data is housed in BambooHR.

Rolling Voluntary Team Member Turnover = (Number of Team Members actively choosing to leave GitLab/Average Total Team Members Count) x 100

Industry Standard Turnover is [22% overall](https://radford.aon.com/insights/infographics/2017/technology/q1-2017-turnover-rates-hiring-sentiment-by-industry-at-us-technology-companies): 15% voluntary and 7% involuntary for software companies.

## Team Member Retention

Team Member Retention = (1-(Number of Team Members leaving GitLab/Average of the 12 month Total Team Member Headcount)) x 100

GitLab measures team member retention over a rolling 12 month period, as well as over a calendar month. (The default period is over a rolling 12 month period.) The 12 month team member retention target is > 84%.   In order to achieve the rolling 12 month team member retention target, the monthly team member total turnover target is < 1.3% (16/12). The data is housed in BambooHR.



## Spend Per Team Member

The spend per team member metric is intended to track variances across the company in compensation, discretionary bonuses, promotions, and involuntary attrition. This metric does not have an associated goal as the purpose is not to reduce costs, but instead understand the early indicators of something going wrong or what may be going well. Consistency should be the key evaluator of the KPI.

### Discretionary Bonuses

1. Calculate Discretionary Bonus program spend per month based on the same process as the [Discretionary Bonuses KPI](/handbook/incentives/#discretionary-bonuses-per-employee)
1. Outline any large deltas and note any takeaways for review at the next monthly metric meeting for People Ops.

### Promotions

* Run the Promotions/Transfers report from BambooHR. Change the Showing from Active to All.
* Set the conditional formatting on the Compensation Change Reason to highlight "Promotion"
* Separate out all data in the month of the review to add to the rolling 12 month totals
* Add columns for the Amount of USD change per year, and the percent increase by determining the values using the record in BambooHR.
* Determine the Promotions Spend Summary by including Rolling 12 Month Payroll Spend (base salary and bonuses), and Average Percent Increase
* Filter this information by Division, Gender, and Ethnicity by creating pivot tables.
  * For the Divisional breakdown, Rolling 12 Month Payroll Spend (salary and bonuses), and Average Percent Increase.
  * For Gender and Ethnicity, the pivot table should calculate the count of those promoted. In a column to the right of the pivot table, use the [Identity Data](/company/culture/inclusion/identity-data/) to determine what percent of the population was promoted.

TODO as a next iteration: Work with Finance to build out a per division budget for promotions to benchmark against.
TODO: evaluate promotion stock grants per month outstanding to be able to look forward on how many options will be needed per quarter for promotions.

### Transfers

* Run the Point in Time Report from BambooHR with the following columns: "Employee Name" "Employee #" "Division" "Department" "Job Title" as of the start of the month
* Run the same report with the end of the month.
* Remove the data from the month no longer part of the rolling 12 month period.
* Add Columns in for Division Match,	Department Match,	Job Title Match and Filter by which line items are false based on the two reports
* Add all Transfers to the "Transfer Analysis Tab" and add the Effective Date by looking it up in the job information table in BambooHR.
* Exclude any data related to organizational moves that the person had no control over. For example, the recruiting department being separated out of People Ops is not a transfer for internal mobility into new roles.
* Create a pivot table to outline the number of transfers into the division in the last rolling 12 months. Add a line for the headcount as of the last day of the rolling 12 month period. Add one more line to take the percentage of transfers into the department.

TODO Next iterations: Outline divisions those are transferring out of, generate a way to analyze compensation implications of transfers (Difficult to report since not all transfers come with a comp change).

### Regrettable Attrition

1. Measure the variance of [PIPs](/handbook/underperformance/#performance-improvement-plan-pip) at GitLab.
  * Run the Employment Status History Report from BambooHR.
  * Sort by Employment Status and filter to "PIP"
  * Use the count function to determine the "Total Number of PIPs at GitLab"
  * Add the following information to the table: PIPs in last rolling 12 months, Number of PIPs Successfully completed, Number of PIPs resulting in a termination
  * Comment on any takeaways based on the data.

We should strive for a PIP success rate of 50% or more.
This indicates that we identity underperformance early when it is easier to remediate than later.
It also indicates to our team members a PIP means we still believe in them and want to make them successful, it isn't a one way street to job termination.

### Compensation

1. Generate a chart for the compa ratio distribution by Division.
  * Using the "Comp Data Analysis & Modeling" Google spreadsheet, copy over the employee ID, first name, last name, and Comp Compa Ratio columns.
  * Using a vlookup, add the division to the report for filtering.
  * Create a pivot table to take the average compa ratio based on Division.
TODO: Filter this analysis by Location, Gender, Ethnicity
TODO: Determine a way to measure roles without an compa ratio (OTE, Director, Exec)
1. Determine the Total Target Compensation (inclusive of OTE) as of the end of the month
  * Using the BambooHR report, take the max of the USD OTE and Annual USD column.
  * Create a pivot table to sum the values by division.
TODO: Create an average spend per headcount.
