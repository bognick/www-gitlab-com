---
layout: handbook-page-toc
title: "GitLab Security Secure Coding Training"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# GitLab Security Secure Coding Training

This page contains information on secure training initiatives sponsored by the GitLab Security team.

## Secure Coding Guidelines

These guidelines cover how to address specific classes of vulnerabilities that have been identified in GitLab.

- [XSS](https://gitlab.com/gitlab-com/gl-security/security-guidelines/blob/master/xss.md)
- [SSRF](https://gitlab.com/gitlab-com/gl-security/security-guidelines/blob/master/ssrf.md)
- [Permissions](https://gitlab.com/gitlab-com/gl-security/security-guidelines/blob/master/permissions.md)
- [Regular Expressions](https://gitlab.com/gitlab-com/gl-security/security-guidelines/blob/master/regex.md)

## Secure Coding Training with Jim Manico

### Description

A developer-focused application security training presented by Jim Manico, and Dr. Justin Collins, the creator of Brakeman, occurred on the days of July 29th and 30th 2019.  In addition to covering secure coding in general, it also covers specific threats and mitigations for Ruby on Rails applications.  The content is presented in a lighthearted and entertaining manner.

You can find the recorded, private YouTube stream at the following:

- [Day 1 Morning](https://www.youtube.com/watch?v=PXR8PTojHmc)
- [Day 1 Afternoon](https://www.youtube.com/watch?v=2VFavqfDS6w)
- [Day 2 Morning](https://www.youtube.com/watch?v=bJYUxKn88so )
- [Day 2 Afternoon](https://www.youtube.com/watch?v=8tP2KVKHO7A)

#### Recommendations

- [Switch account in YouTube](https://support.google.com/youtube/answer/3046356?co=GENIE.Platform%3DDesktop&hl=en) to the GitLab Unfiltered account or the videos will not be accessible.
- The videos were recorded over two full days.  It is suggested that you split up viewing them over multiple days by topic and/or by the hour.
- If you prefer to view the slides vs watching the video on some topics, note that the slides are available ([see link below](#additional-resources)).
- Consider watching the videos at [1.25X speed ](http://osxdaily.com/2017/04/14/adjust-youtube-video-playback-speed/)

### Schedule and Topics

#### Day 1

##### Day 1 Morning
  1. Introduction to Application Security (4:33)
  1. Threat Modeling 
  1. OWASP Top Ten 2017 overview (42:57)
  1. A1: Injection (52:03)
  1. A2: Broken Authentication and Session Management (1:19:50)
  1. A7: Cross site scripting - XSS (2:09:45)
  1. A8: Insecure deserialization (2:15:10)
  1. A9: Using known vulnerable components (2:22:26)
  1. A10: Insufficient logging and monitoring (2:24:30)

Also covers: 
  *  OWASP ASVS 4.0
  *  Multi-Form Workflow Security

#####  Day 1 Afternoon

  1. XSS Defense - HAML (1:51)
  1. Safe client-side JSON Handling (1:45:31)
  1. iFrame Sandboxing (1:57:25)
  1. Input validation (2:04:50)
  1. Unvalidated Redirects (2:22:14)
  1. DevOps Best Practices (3:14:30)
  1. Content Security Policy (3:36:31)
  1. Brakeman and Static Analysis (4:09:20)

Also covers: 
 * DevOps Best Practices ​
 * Coding Vue.js applications securely
 * File Upload and File IO Security ​Multi-Step Secure File Upload Defense, File I/O Security
 * Input Validation ​Basics ​(Whitelist Validation​ and Safe Redirects)
 * 3rd Party Library Security Management ​(​Detect and manage insecure 3rd party libraries)

#### Day 2

##### Day 2 Morning

  1. Access control (4:28)
  1. Insecure direct object reference in Rails (58:20)
  1. Cross site request forgery (1:28:33)
  1. Cross site request forgery protection in Rails (1:52:32)
  1. Cookie Options and Security (2:33:45)
  1. Server Side Request Forgery SSRF (2:44:22)

Also covers: 
 * Dynamic render paths and local file inclusion
 * IDOR and scoped queries
 * SSRF Defense
 * Cross Site Request Forgery CSRF Defenses for multiple architecture types (stateless, API,web, etc)

##### Day 2 Afternoon
  1. Authentication Best Practices (5:40)
  1. Rails 6 Security Features (2:23:15)
  1. Introduction to the OAuth authorization protocol v1 (2:48:21)
  1. OAuth v2 (2:51:05)
  1. Client Registration (3:04:06)
  1. Authorization Code Grant (3:07:44)
  1. OAuth 2.0 Terminology (3:21:06)
  1. OAuth 2.0 Tokens (3:35:35)

Also covers: 
* Secure Secret Storage
* Encrypted secrets/credentials

## Recommended topics by role

### Frontend Engineers

* Day 1
  * Intro to application security
  * Threat modeling
  * OWASP top 10 overview
  * Injection
  * Broken authentication
  * Cross site scripting
  * Insecure deserialization
  * Using vulnerable components
  * XSS defense
  * Safe client side JSON handling
  * iFrame sandboxing
  * Unvalidated redirects
  * Content security policy
  * Brakeman and static analysis
* Day 2
  * Access control
  * Cross site request forgery
  * Cookie options and security
  * SSRF
  * Authentication
  * Rails6 security features
  * OAuth

### Backend Engineers

* Day 1
  * Intro to application security
  * Threat modeling
  * OWASP top 10 overview
  * Authentication and session management
  * Insecure deserialization
  * Vulnerable components
  * Logging & monitoring
  * XSS defense
  * Input validation
  * DevOps best practices
  * Brakeman
* Day 2
  * Access control
  * Insecure direct object reference
  * SSRF
  * Authentication best practice
  * Rails 6 security features
  * OAuth

### SRE:
* Day 1
  * Intro to application security
  * Threat modeling
  * OWASP top 10 overview
  * DevOps Best practices

### Additional resources

- [PowerPoint presentations](https://drive.google.com/drive/folders/1NRrlnqwkhsS-UmuagwoD8GB4APXsfJxb?usp=sharing)
- [Burp Proxy](https://portswigger.net/burp/communitydownload)
- [Online Labs](https://manicode.us/shepherd/)
- [Questions Doc](https://docs.google.com/document/d/1KsK5DBDgiF8k0N3cs89o1VsMYsUWUPH9fIQb_smFEac/edit)
