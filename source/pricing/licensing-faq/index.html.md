---
layout: markdown_page
title: Licensing and subscription FAQ
---

### On this page
{:.no_toc}

{:toc}
- TOC

### How do I purchase a subscription?

You can purchase a subscription for GitLab Enterprise Edition (self-managed) or for
GitLab.com (hosted by GitLab) on our <a href="https://customers.gitlab.com/" target="_blank">Customers Portal</a>
with a credit card.

___

### How much does GitLab cost?

You can find pricing for GitLab.com subscriptions on our <a href="https://about.gitlab.com/pricing/" target="_blank">pricing page here</a> and GitLab Enterprise Edition (self-managed) <a href="https://about.gitlab.com/pricing/#self-managed" target="_blank">here</a>.

___

### Can I pay for the subscription monthly?

All subscriptions are paid in annual payments, monthly payments are not an available payment option. 

___

### What payment methods are accepted?

When purchasing via our <a href="https://customers.gitlab.com/customers/sign_in" target="_blank">Customers Portal</a> you may pay via credit card. We are able to accept payment via check and wire in select circumstances. To learn more, contact support via <a href="https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000071293" target="_blank">our support form</a> and select **Licensing and Renewals Problems** from the form menu.

___

### How do I use my subscription?

Please take a look at our <a href="https://docs.gitlab.com/ee/getting-started/subscription.html" target="_blank">getting started with subscriptions page<a/> for information on getting set up, applying, and managing your subscription.

___ 

### How do I renew my subscription?

#### GitLab.com
{:.no_toc}

1. Log into your account in the <a href="https://customers.gitlab.com/customers/sign_in" target="_blank">Customers Portal</a>.
1. Select **Manage Purchases** from the menu.
1. Toggle `ON` the **Auto Renew** setting.
1. Note, the renewal will automatically occur at 12:00am UTC on the expiration date listed and will be processed for the exact subscription details (tier and number of users) as the previous subscription. If you require a change to the number of users or tier, please contact support via <a href="https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000071293" target="_blank">our support form</a> and select **Licensing and Renewals Problems** from the form menu.

#### Self-managed GitLab
{:.no_toc}

1. Log into your account in the <a href="https://customers.gitlab.com/customers/sign_in" target="_blank">Customers Portal</a>.
1. Select **Manage Purchases** from the menu.
1. Select the **Renew** button.
1. Determining user counts for renewal purposes:
    1. **Users** Only active users at the time of renewal count towards total user count for the upcoming subscription.
    1. **Users over license** (aka true-up users) If at any time during the prior subscription period the account has had more billable users than the subscription, these users over the license will be due at the time of renewal.
1. Select **Proceed to checkout**.
1. Review Subscription Renew Detail.
1. Select on **Confirm Renew**.

The following will be emailed to you:
- A payment receipt. You can also access this information in the <a href="https://customers.gitlab.com/customers/sign_in" target="_blank">Customers Portal</a> under **Payment History**.
- A new license. <a href="https://docs.gitlab.com/ee/user/admin_area/license.html#uploading-your-license" target="_blank">Upload this license</a> to your instance to use it.

##### Administrators of self-managed instances can find user usage through the following options:
{:.no_toc}
1. Within GitLab UI, select `Admin Area -> Users` to view the Active Users tab which indicates the users currently
counted.
1. View the User Statistics panel from `Admin Area -> Overview -> Dashboard` to view users available in license and users over license.
1. Run the command `sudo gitlab-rails runner 'p User.active.count'` to obtain the Active User count.
1. Run the command `sudo gitlab-rails runner 'p ::HistoricalData.max_historical_user_count'` to obtain the Maximum billable user count.
1. Run the command `GET /users` to obtain a list of all billable users.

___

### What does "users over license" mean?

If you've added more users to your GitLab EE instance during the past period
than you were licensed for, the additional users will be payable at the time of renewal.

Without adding these users during the renewal process, your license key will
not work.

You can find the number of **users over license** by going to the `/admin`
section of your GitLab instance (e.g. `https://example.gitlab.com/admin`). This
can also be found by clicking the **admin wrench** in the navbar of your instance
when logged in as an admin.

In the top right section of the admin dashboard, you should see the number to
enter when asked this during the renewal process.

___
### Who gets counted in the subscription?

#### GitLab.com
{:.no_toc}
Every occupied seat, whether by person, job or bot is counted in the subscription. The only exception are <a href="https://docs.gitlab.com/ee/user/permissions.html#free-guest-users-ultimate" target="_blank">members with `Guest` permissions with an Gold subscription</a>. 

Since GitLab.com counts concurrent seats and not named users, you can remove members and add new members as you'd like as long as the total users at any given time is within your license count.


#### Self-managed GitLab
{:.no_toc}
Every occupied seat, whether by person, job or bot is counted in the subscription. 

The following are the only exceptions which are not counted towards the subscription:
1. Blocked users who are blocked prior to the renewal of a subscription will **not** be counted as Active Users for the renewal subscription but **may** count as true-up users for the term in which they were originally added.
1. <a href="https://docs.gitlab.com/ee/user/permissions.html#free-guest-users-ultimate" target="_blank">Members with `Guest` permissions on an Ultimate subscription</a> do not count towards the subscription.
1. <a href="https://docs.gitlab.com/ee/user/profile/account/delete_account.html#associated-records" target="_blank">Ghost User</a> and <a href="https://docs.gitlab.com/ee/user/project/service_desk.html#support-bot-user" target="_blank">Support Bot</a> do not count towards the subscription.

___

### Do I need the administrator account (aka `root`) that came installed if I am also an administrator?

Nope! The `root` user is just the first admin account, created by default in self-managed versions of GitLab. Just like any other user, this account _does_ occupy a license seat. So, please consider following good security practice and have one or more "real" people play the role of administrator. You're allowed (and encouraged) to rename the user, or even delete or disable it as long as you've promoted other users to administrator. 

___

### How do I start a trial?

It is possible to obtain a free evaluation of our GitLab.com or self-managed subscriptions for
a 30 day period for up to 100 users. Please visit our
<a href="https://about.gitlab.com/free-trial/" target="_blank">free trial page</a> to sign up.

For self-managed users, when you decide to purchase a subscription, you will be issued a new license
key. Should you not take out a subscription, your key will expire at the end
of your evaluation period. At that point you should remove the trial key and the system will revert to our free Core version.

___

### Can I use my paid seats for different users?

The seats for your license are generic and are not specific to a user. GitLab does
not use a named license model.

The seats you buy can be distributed however you choose. If a user leaves your
organization, you can remove or block that user to free the seat. This seat can
then be used by another user.

Note that this may result in a user over license if your maximum users has been reached.

___

### Can I add more users to my subscription?

#### GitLab.com
{:.no_toc}

Currently we do not have a self-service option to for adding additional users to your subscription outside of initial purchase and renewal periods. 
You can add users to your GitLab.com group as needed, however you won't be billed for these until the renewal cycle. 
If you would like to be billed offcycle for additional users please please contact support via <a href="https://support.gitlab.com/hc/en-us/requests/new" target="_blank">our support form</a> and select **Licensing and Renewals Problems** from the form menu.

#### Self-managed GitLab
{:.no_toc}

You can add users to your subscription any time during the subscription period. The cost of additional users added during the subscription period will be prorated from the date of purchase through the end of
the subscription period. 

To do this:
1. Log into your account via the <a href="https://customers.gitlab.com/customers/sign_in" target="_blank">Customers Portal</a> 
1. Select **Manage Purchases** from the menu.
1. Select the **Add more seats** button.
1. Enter the amount of additional users (E.g. You currently have 10 users and want to add 5 more users, enter 5).
1. Select **Proceed to checkout**.
1. Review the Subscription Upgrade Detail. Note, the system has listed the total price for all users on the system and a credit for what you've already paid. You will only be charged for the net change.
1. Select **Confirm Upgrade**

The following will be emailed to you:
- A payment receipt. You can also access this information in the <a href="https://customers.gitlab.com/customers/sign_in" target="_blank">Customers Portal</a> under **Payment History**.
- A new license. <a href="https://docs.gitlab.com/ee/user/admin_area/license.html#uploading-your-license" target="_blank">Upload this license</a> to your instance to use it.


___

### Do I need an additional license if I run more than one server (e.g., for backup, high availability, failover, testing, and staging)?

No, if your GitLab servers cover the same users, you can use
the same license file for all of them.

___

### What happens when my subscription is about to expire or has expired?

#### GitLab.com
{:.no_toc}

Once the subscription has expired, the system will revert to the Free tier and users will no longer see paid features, however no data will be lost.


#### Self-managed GitLab
{:.no_toc}

- Starting 30 days before the subscription end date, GitLab will display a
  notice to all administrators informing them of the impending expiration.
- On the day the subscription expires, nothing changes.
- 14 days after the subscription has ended, GitLab will display a notice to all
  users informing them of the expiration, and pushing code and creation of
  issues and merge requests will be disabled. The system will <a href="https://docs.gitlab.com/ee/user/admin_area/license.html#what-happens-when-your-license-expires" target="_blank">not be functional anymore</a>.


___

### What's the difference between a Group plan and a Personal plan on GitLab.com?

A subscription for GitLab.com can be applied to one of two types of namespaces. Where you assign your subscription determines where those features are accessible.

#### GitLab.com Plan on Personal Namespace
{:.no_toc}

If a subscription is applied to a personal account then that account will have access to the features of the subscription for all of the projects 
they create under their personal account. If they want, other users can be invited to those projects and they'll be able to enjoy the features of that subscription 
_only while working on those projects_. If that user then creates a group, it will by default be on the **Free plan**.

A user that never collaborates with others on GitLab may opt to purchase a subscription for their personal user account since they have no need for a group and will always only work in projects under their personal account.

You can find the plan details for a personal namespace by navgiating the **User Settings>Account>Billing**.


#### GitLab.com Plan on a Group
{:.no_toc}

A user can choose to purchase a subscription and apply it to a group they've created. This way any project they create in that group or in a 
subgroup of that group gets access to the features of the subscription they purchased for it. This extends to any user that gets invited as a member of that group.

A user that's part of an organization with multiple GitLab collaborators will ideally choose to create a group for that organization, purchase and apply a subscription to that group, and then invite their colleagues to that group so that all can 
enjoy those paid features while working in that group.

Note that all members within a group subscription are counted as billable seats at the same subscription plan (Bronze, Silver, Gold) rate.

You can find the plan details for a group namespace by navgiating the **Group Settings>Account>Billing**.


___

### How do I purchase a Group Plan on GitLab.com?

You will first need to create your group in GitLab.com and add users. Follow the steps below:

1. <a href="https://docs.gitlab.com/ee/user/group/#create-a-new-group" target="_blank">Create a group</a> in GitLab.com
1. <a href="https://docs.gitlab.com/ee/user/group/#add-users-to-a-group" target="_blank">Add users to the group</a> 
1. Log into the <a href="https://customers.gitlab.com/customers/sign_in" target="_blank">Customers Portal</a> to purchase the desired plan for your group. 
1. Select the GitLab.com subscription plan using the **Order (Bronze, Silver, Gold) Plan** button 
1. From the **This subscription is for** dropdown, select the group name you've created
1. Select the **Proceed to checkout** button


___

### How do I downgrade my subscription?

Please submit your request via <a href="https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000071293" target="_blank">our support form</a> and select **Licensing and Renewals Problems** from the form menu. 
Note, only purchases made within the last 45 days are eligible for a refund.

___

### How can I get a copy of my invoice?

Your invoice should be available for viewing and download within our <a href="https://customers.gitlab.com/customers/sign_in" target="_blank">Customers Portal</a> by navigating to the **Purchase History** page. If your invoice is not available, please submit your request via <a href="https://support.gitlab.com/hc/en-us/requests" target="_blank">our support form</a> and select **Payment Failures and Refund** from the menu.

___

### I purchased a multi-year subscription, why is my license only for 1 year?

We issue self-managed GitLab licenses in 12 month increments and check-in at the start of each subsequent term within the subscription period to see if there are any changes to users needed prior to generating the license. 

You can always contact us via <a href="https://support.gitlab.com/hc/en-us/requests" target="_blank">our support form</a> as the next term approaches otherwise someone will be in touch as well.


___

### Do you support resellers?

We don't currently support reseller purchasing via the portal. If you are a
reseller looking to purchase GitLab on behalf of your client, please get in
touch with us using the <a href="https://about.gitlab.com/sales/" target="_blank">Contact sales form</a>.

If you include your billing contact name and email, your physical billing
address, and the end customer's name, email address and shipping address, we
will send you (not your customer) a resellers quote which you can execute
either with a credit card or an EFT.

You can find details on our reseller program at https://about.gitlab.com/resellers/program.
